#include <iostream>
#include "strom.h"
#define COUNT 10




struct strom* new_data(int data) {
    struct strom* new_data = (struct strom*)malloc(sizeof(struct strom));
    new_data->data = data;
    new_data->right = NULL;
    new_data->left = NULL;
    return new_data;
}

struct strom* add_data(struct strom* root, int data) {
    if (root == NULL) {
        root = new_data(data);
        return root;
    }
    else if (root->data < data) {
        root->right = add_data(root->right, data);
    }
    else if (root->data > data) {
        root->left = add_data(root->left, data);
    }
    return root;
}
struct strom* minimum(struct strom* root) {
    struct strom* aktualni = root;
    while (aktualni != NULL and aktualni->left != NULL)
    {
        aktualni = aktualni->left;
    }
    return aktualni;
}


struct strom* Delete(struct strom* root, int data) {
    if (root == NULL)
        return root;
    else if (root->data < data) {
        root->right = Delete(root->right, data);
    }
    else if (root->data > data) {
        root->left = Delete(root->left, data);
    }
    else if (root->data == data) {
        if (root->right == NULL and root->left == NULL) {
            free(root);
            root = NULL;
            return root;
        }
        else if (root->left == NULL) {
            struct strom* tmp = root;
            root = root->right;
            free(tmp);
            return root;
        }
        else if (root->right == NULL) {
            struct strom* tmp = root;
            root = root->left;
            free(tmp);
            return root;
        }
        else {
            struct strom* tmp = minimum(root->right);
            root->data = tmp->data;
            root->right = Delete(root->right, tmp->data);
        }
    }
    return root;
}

void print(struct strom* root, int space)
{

    if (root == NULL)
        return;
    space += COUNT;

    print(root->right, space); //tiskneme prvou vetev 

    printf("\n");
    for (int i = COUNT; i < space; i++)
        printf(" ");
    printf("%d\n", root->data);

    print(root->left, space);// tiskneme levou vetev 
}
void print_strom(struct strom* root)
{

    print(root, 0);
}