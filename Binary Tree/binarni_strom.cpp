
#include <iostream>
#include "strom.h"

struct strom* root = NULL;

int main()
{
    int operace, hodnota;
    printf("1:pridat ; 2:smazat\n");
    scanf_s("%d", &operace);
    do {
        switch (operace)
        {
        case pridat:
            printf("Uvedte jakou hodnotu chcete pridat\n ");
            scanf_s("%d", &hodnota);
            root = add_data(root, hodnota);
            break;
        case smazat: printf("Uvedte jakou hodnotu chcete smazat\n ");
            scanf_s("%d", &hodnota);
            Delete(root, hodnota);
            break;
        default:
            break;
        }
        printf("Co dal?\n");
        scanf_s("%d", &operace);
    } while (operace != 0);


    print_strom(root);


    return 0;
}