#pragma once


struct strom {
    int data;
    struct strom* right;
    struct strom* left;
};

struct strom* new_data(int data); 

struct strom* add_data(struct strom* root, int data); 

struct strom* minimum(struct strom* root); 

struct strom* Delete(struct strom* root, int data); 

void print(struct strom* root, int space); 

void print_strom(struct strom* root); 

enum op { pridat = 1, smazat, };
